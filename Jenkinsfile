def docker_images = ["conangcc48", "conangcc54"]
// def docker_images = ["conangcc48", ]

def get_stages(docker_image) {

    stages = {
        docker.image(docker_image).inside {
            stage("${docker_image}") {
                echo "Running in ${docker_image}"
            }

            stage("Upload packages") {
                switch (docker_image) {
                    case "conangcc48":
                        // let's set environment variables
                        env.CONAN_USER_HOME = "/tmp/${docker_image}"
                        sh """
                            sed -i  "s|\\<version =.*|version = \'`echo ${params.tau_version}|sed 's/v//g'`\'|g" conanfile.py
                            cat conanfile.py

                        """
                    case "conangcc54":
                        // let's set environment variables
                        env.CONAN_USER_HOME = "/tmp/${docker_image}"
                        sh """
                            sed -i  "s|\\<version =.*|version = \'`echo ${params.tau_version}|sed 's/v//g'`\'|g" conanfile.py
                            cat conanfile.py
                        """
                    default:
                        env.CONAN_USER_HOME = "${HOME}"
                }
            }
        }
    }
    return stages
}

// Every jenkins file should start with either a Declarative or Scripted Pipeline entry point.
node('master') {
    // Utilizing a try block so as to make the code cleaner and send slack notification in case of any error
    try {

        // Pull the code from the repo
        checkout scm

        def stages = [:]
        for (int i = 0; i < docker_images.size(); i++) {
            def docker_image = docker_images[i]
            stages[docker_image] = get_stages(docker_image)
        }

        // let's execute stages in parallel.
        parallel stages
    }
    catch (e) {
            currentBuild.result = "FAILED"
            throw e
    }
    finally {
            notifyBuild(currentBuild.result)
    }
}

def notifyBuild(String buildStatus = 'STARTED') {
  buildStatus =  buildStatus ?: 'SUCCESSFUL'

  def channel = '@dhanasekaran'
  def color = 'RED'
  def colorCode = '#FF0000'
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} (${env.BUILD_URL})"
  def details = """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>"""

  if (buildStatus == 'STARTED') {
    color = 'YELLOW'
    colorCode = '#FFCC00'
  } else if (buildStatus == 'SUCCESSFUL') {
    color = 'GREEN'
    colorCode = '#228B22'
  } else {
    color = 'RED'
    colorCode = '#FF0000'
  }

  slackSend (channel: channel, color: colorCode, message: summary)
}