import os
from conans import ConanFile, CMake, tools


class TauConan(ConanFile):
    name = "tau"
    version = "master"
    git_version = "v0.2.13"
    license = "<Put the package license here>"
    url = "bitbucket.org/estmore_india_dev/tau.git"
    description = "<Description of Tau here>"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "version": "ANY"}
    default_options = "shared=False", "version=master"
    generators = "cmake"
    no_copy_source = True
    requires = 'Poco/1.9.0@eastmore/incubator', 'quickfix/1.14.4@eastmore/incubator', 'rpclib/2.2.1@eastmore/incubator', \
               'boost/1.66.0@eastmore/incubator', 'catch2/2.2.2@eastmore/incubator', 'zlib/1.2.11@eastmore/incubator', \
               'kdb-c/0.0.1@eastmore/incubator', 'OpenDDS/3.12@eastmore/incubator', 'ACE/3.12@eastmore/incubator', \
               'TAO/3.12@eastmore/incubator'

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", src="lib", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)
        self.copy("*.hpp", src="Tau/include", dst="include", keep_path=True)
        self.copy("*.h", src="Tau/include", dst="include", keep_path=True)

    def package_info(self):
        self.cpp_info.includedirs = ['include']  # Ordered list of include paths
        self.cpp_info.libs = tools.collect_libs(self)  # The libs to link against
        if self.options.shared:
            self.env_info.LD_LIBRARY_PATH.append(os.path.join(self.package_folder, "lib"))
